<?php

return [
    'messaging' => 'PERPESANAN',
    'compose' => 'Buat Pesan',
    'inbox' => 'Kotak Masuk',
    'sentbox' => 'Kotak Terkirim',

    'account' => 'AKUN',
    'logout' => 'Keluar',
];
