<?php

return [
    'confirmation' => [
        'delete' => 'Anda yakin menghapus :attribute ?',
        'logout' => 'Anda yakin keluar?',
    ],

    'message' => [
        'sent' => ':attribute berhasil dikirim.',
        'delete' => ':attribute berhasil dihapus.',
    ],
];