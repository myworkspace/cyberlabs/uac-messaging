<?php

return [

    'fields' => [
        'from_email' => 'Dari Email',
        'destination_email' => 'Tujuan Email',
        'destination_email_note' => 'Pisahkan email dengan koma',
        'subject' => 'Judul',
        'content' => 'Konten',
        'date' => 'Tanggal',
        'action' => 'Aksi',
    ],

    'message' => 'Pesan',

];