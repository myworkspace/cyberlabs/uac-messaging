<?php

return [

    'login' => 'Login',
    'add' => 'Tambah',
    'edit' => 'Sunting',
    'delete' => 'Hapus',
    'update' => 'Perbarui',
    'save' => 'Simpan',
    'detail' => 'Detail',
    'back' => 'Kembali',
    'send' => 'Kirim',
    
];