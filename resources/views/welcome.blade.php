@extends('layouts.app')

@section('body')

<div class="row justify-content-center">
    <div class="col-xl-5">
        <div class="card py-4 px-3">
            <div class="card-body">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <img src="{{ asset('images/logo.png') }}" class="mb-4 w-100">
                    <h3 class="mb-5 text-center">{{ config('app.name') }}</h3>
                    <button type="submit" class="btn btn-primary btn-block">@lang('buttons.login')</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection