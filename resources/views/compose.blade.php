@extends('layouts.dashboard')

@section('content')

<div class="row mb-4">
    <div class="col-lg-10">
        <h2>@lang('menus.compose')</h2>
    </div>
</div>

<form method="post" action="" id="form">

    @csrf

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label for="emails">@lang('pages.fields.destination_email')</label>
                <input name="emails" type="text" class="form-control{{ $errors->has('emails') ? ' is-invalid' : '' }}" id="emails" value="{{ old('emails') ? old('emails') : '' }}" required>
                <small class="text-muted">@lang('pages.fields.destination_email_note')</small>
                @if ($errors->has('emails'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('emails') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="subject">@lang('pages.fields.subject')</label>
                <input name="subject" type="text" class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" id="subject" value="{{ old('subject') ? old('subject') : '' }}" required>
                @if ($errors->has('subject'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('subject') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="content">@lang('pages.fields.content')</label>
                <div class="content-area">
                    <textarea name="content" rows="5" class="editor form-control{{ $errors->has('content') ? ' is-invalid' : '' }}" id="content" value="{{ old('content') ? old('content') : '' }}" required></textarea>
                </div>
                @if ($errors->has('content'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('content') }}</strong>
                </span>
                @endif
            </div>

            <button type="button" onclick="submitForm();" id="button" class="btn btn-primary mt-4">{{ __('buttons.send') }}</button>
        </div>
    </div>

</form>

@endsection

@section('javascript')
<script>
    $('.editor').trumbowyg({
        btns: [
            ['undo', 'redo'], // Only supported in Blink browsers
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link'],
            ['insertImage'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['fullscreen']
        ]
    });
</script>
@endsection