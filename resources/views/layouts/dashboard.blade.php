@extends('layouts.app')

@section('body')

<div class="row">
    <div class="col-lg-3">
    
    <nav class="navbar navbar-expand-lg navbar-light flex-lg-column p-0 text-left">
        <a class="navbar-brand" href="#">
            <img src="{{ asset('images/logo-small.png') }}" class="logo" />
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse mt-3" id="navbarNav">
            <ul class="navbar-nav flex-lg-column">
                <li class="nav-item">
                    <small class="nav-link text-muted">@lang('menus.messaging')</small>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->segment(1) == 'compose' ? 'active' : '' }}" href="{{ route('compose') }}">@lang('menus.compose')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->segment(1) == 'inbox' ? 'active' : '' }}" href="{{ route('box', ['type' => 'inbox']) }}">@lang('menus.inbox') <span class="badge badge-secondary">{{ request()->unread }}</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->segment(1) == 'sentbox' ? 'active' : '' }}" href="{{ route('box', ['type' => 'sentbox']) }}">@lang('menus.sentbox')</a>
                </li>
                <li class="nav-item mt-4">
                    <small class="nav-link text-muted text-uppercase">{{ request()->user['name'] }}</small>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}" onclick="return confirm('@lang('alert.confirmation.logout')'); return false;">@lang('menus.logout')</a>
                </li>
            </ul>
        </div>
    </nav>

    </div>
    <div class="col-lg-9 mt-4 mt-lg-0">
        @yield('content')
    </div>
</div>

@endsection
