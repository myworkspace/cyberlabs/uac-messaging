@extends('layouts.dashboard')

@section('content')

<div class="row mb-4">
    <div class="col-lg-10">
        <h2>{{ $title }}</h2>
    </div>
</div>

@if(session('success'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">×</button>
	{!! session('success') !!}
</div>
@endif

<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">@lang('pages.fields.date')</th>
				<th scope="col">@lang('pages.fields.subject')</th>
				<th scope="col">{{ $type == 'inbox' ? __('pages.fields.from_email') : __('pages.fields.destination_email') }}</th>
				<th scope="col" class="column-action-2 text-center">@lang('pages.fields.action')</th>
			</tr>
		</thead>
		<tbody>
		@foreach($boxes as $box)
			<tr class="{{ $box->is_read == 0 ? 'font-weight-bold' : '' }}">
				<th scope="row">{{ ++$num }}</th>
				<td>{{ $box->created_at->format('d-m-Y H:i:s') }}</td>
				<td>{{ $box->subject }}</td>
				<td>{{ $type == 'inbox' ? $box->user_email : $box->emails }}</td>
				<td class="text-right">
                    <a href="{{ route('detail', ['id' => $box->id, 'type' => $type]) }}" class="btn btn-sm btn-outline-info">@lang('buttons.detail')</a>

                    <form style="display: inline" id="form-{{ $num }}" action="{{ route('delete', ['id' => $box->people_id]) }}" method="post">
                        @method('DELETE')
                        @csrf
                        <button type="button" onclick="submitForm('#form-{{ $num }}', '#delete-{{ $num }}', '@lang('alert.confirmation.delete', ['attribute' => $box->subject])');"  id="delete-{{ $num }}" class="btn btn-sm btn-outline-danger">@lang('buttons.delete')</button>
                    </form>
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>

@endsection
