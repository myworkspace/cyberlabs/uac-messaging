@extends('layouts.dashboard')

@section('content')

<div class="row mb-4">
    <div class="col-lg-10">
        <h2>{{ $message->subject }}</h2>
    </div>
</div>

<form method="post" action="" id="form">

    @csrf

    <div class="form-group row">
        <label for="staticEmail" class="col-sm-2 col-form-label">@lang('pages.fields.from_email')</label>
        <div class="col-sm-10">
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{ $message->user_email }}">
        </div>
        <label for="staticEmailTo" class="col-sm-2 col-form-label">@lang('pages.fields.destination_email')</label>
        <div class="col-sm-10">
            <input type="text" readonly class="form-control-plaintext" id="staticEmailTo" value="{{ $message->emails }}">
        </div>
    </div>

    <div class="row mt-4 content-area">
        <div class="col-sm-12">
            <div class="form-group">
                {!! $message->content !!}
            </div>
        </div>
    </div>

</form>

@endsection