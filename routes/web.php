<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', function () {
    if(session('token'))
        return redirect('inbox');
    return view('welcome');
});

Route::get('/logout', function(){
    $token = session('token');
    session()->forget('token');
    session()->flush();
    return redirect()->to(env('BKM_URL_LOGOUT') . "?token=" . $token . "&code=" . env('BKM_CODE'));
})->name('logout');

Route::post('/login', 'LoginController@login')->name('login');
Route::get('/login', 'LoginController@receiver')->name('login.receiver');

Route::group(['middleware' => 'auth.custom'], function(){

    Route::get('/compose', function(){
        return view('compose');
    })->name('compose');
    
    Route::post('/compose', 'MessageController@send')->name('send');
    Route::get('/{type}', 'MessageController@box')->name('box');
    
    Route::get('/{type}/{id}', 'MessageController@detail')->name('detail');
    Route::delete('/{id}', 'MessageController@delete')->name('delete');

});
