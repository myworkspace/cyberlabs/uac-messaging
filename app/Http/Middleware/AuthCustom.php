<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\People;

class AuthCustom
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $request->session()->has('token')) {
            return redirect('/');
        } else {
            // get user data
            $params['token'] = $request->session()->get('token');
            $user = curl(env('BKM_URL_DATA'), $params);

            if($user == null){
                return redirect('logout');
            } else {
                $request->request->add([
                    'user' => [
                        'email' => $user->email,
                        'name' => $user->name,
                        'level' => $user->level_id,
                    ],
                ]);

                $user_email = $user->email;
                $unread = People::getUnread($user_email);
                $unread = $unread > 0 ? $unread : 0;
                $request->request->add(['unread' => $unread]);
            }
        }
        return $next($request);
    }
}
