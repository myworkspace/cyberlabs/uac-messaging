<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Message;
use App\Models\People;

use Validator;

class MessageController extends Controller
{

    public function send(Request $request){

        $email = $request->user['email'];

        $validator = Validator::make($request->all(), [
            'emails' => 'required',
            'subject' => 'required',
            'content' => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect()
                    ->route('compose')
                    ->withErrors($validator)
                    ->withInput();
        }

        $emails = $request->input('emails');

        $create = [
            'subject' => $request->input('subject'),
            'content' => $request->input('content'),
            'emails' => $emails,
        ];

        $message_id = Message::create($create)->id;

        $create = [
            'message_id' => $message_id,
            'user_email' => $email,
            'type' => 1,
            'is_read' => 1,
        ];

        People::create($create);

        $explode = explode(',', $emails);
        foreach($explode as $email){
            $create = [
                'message_id' => $message_id,
                'user_email' => trim($email),
                'type' => 2,
            ];

            People::create($create);
        }

        return redirect()
                ->route('box', ['type' => 'sentbox'])
                ->withSuccess(__('alert.message.sent', ['attribute' => __('pages.message')]));

    }
    
    public function box(Request $request, $type){

        $email = $request->user['email'];

        $title = __('menus.' . $type);
        
        $page = $request->input('page');
        $limit = 15;
        $num = 0;
        if($page > 0) $num = ($page - 1) * $limit;

        $type_id = $type == 'inbox' ? 2 : 1;
        $messages = Message::getBox($email)->where('people.type', $type_id)->orderBy('id', 'desc')->paginate($limit);

        return view('box', [
            'title' => $title,
            'boxes' => $messages,
            'type' => $type,
            'num' => $num,
        ]);

    }

    public function detail(Request $request, $type, $id){

        $message = Message::getDetail()
                    ->where('user_email', $request->user['email'])
                    ->findOrFail($id);

        if($message->is_read == 0){
            $update = ['is_read' => 1];
            People::where('user_email', $request->user['email'])->update($update);
            $unread = $request->unread - 1;
            $request->request->add(['unread' => $unread]);
        }

        return view('detail', [
            'message' => $message,
        ]);

    }

    public function delete($id){

        try {
            People::where('id', $id)->delete();
            return back()
                ->withSuccess(__('alert.message.delete', ['attribute' => __('pages.message')]));
        }
        catch (\Exception $e) {
            return back()
                ->withError(__('alert.error.delete', ['attribute' => __('pages.message')]));
        }

    }

}
