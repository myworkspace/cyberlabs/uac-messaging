<?php

if (!function_exists('formatCurrency')) {
    function formatCurrency($number, $symbol = false, $decimal = 2){
        return ($symbol ? "Rp. " : "") . rtrim(rtrim(number_format($number, $decimal, ",", "."), '0'), ',');
    }
}

if (!function_exists('formatNumber')) {
    function formatNumber($number, $decimal = 0){
        return number_format($number, $decimal, ".", "");
    }
}

if (!function_exists('jsonResponse')) {
    function jsonResponse($data, $message = "ok", $code = 200){
        $response = [
            'code' => $code,
            'message' => $message,
            'data' => $data,
        ];

        return response()->json($response, $code);
    }
}

if (!function_exists('formatResponse')) {
    function formatResponse($request, $data, $view = null){
        
        if($request->wantsJson() || $request->ajax()){
            return jsonResponse($data);
        } else {
            return view($view, $data);
        }

    }
}

if (!function_exists('curl')) {
    function curl($url, $params){
        
        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch,CURLOPT_VERBOSE, 1);
        curl_setopt($ch,CURLOPT_HTTPHEADER, [
            'Accept: application/json',
        ]);

        //execute post
        $result = curl_exec($ch);

        //get info
        $info = curl_getinfo($ch);
        $http_code = $info['http_code'];

        //close connection
        curl_close($ch);

        $respon = null;
        if($http_code == 200)
            $respon = json_decode($result);

        return $respon;

    }
}

