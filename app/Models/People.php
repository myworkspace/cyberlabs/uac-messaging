<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class People extends Model
{
    
    protected $fillable = [
        'message_id',
        'user_email',
        'type',
        'is_read',
    ];

    public function message(){
        return $this->belongsTo('App\Models\Message', 'message_id', 'id');
    }

    public function scopeGetUnread($query, $user_email){

        $fields = DB::raw('COUNT(id) AS unread');
        $unread = $query->select($fields)
                            ->where('is_read', '=', 0)
                            ->where('type', '=', 2)
                            ->where('user_email', '=', $user_email)
                            ->first()
                            ->unread;

        return $unread;

    }


}
