<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Message extends Model
{
    
    protected $fillable = [
        'subject',
        'content',
        'emails',
        'is_read',
    ];

    public function people(){
        return $this->hasMany('App\Models\People', 'message_id', 'id');
    }

    public function scopeGetBox($query, $email){

        $fields = DB::raw('messages.*, people.id AS people_id, people.user_email, people.is_read');
        $result = $query->select($fields)
                            ->join('people', 'people.message_id', 'messages.id')
                            ->where('people.user_email', '=', $email);

        return $result;

    }

    public function scopeGetDetail($query){

        $fields = DB::raw('messages.*, people.user_email, people.is_read');
        $result = $query->select($fields)
                            ->join('people', 'people.message_id', 'messages.id');

        return $result;

    }

}
