function submitForm(formId, buttonId, confirmation){

    formId = formId ? formId : '#form';
    buttonId = buttonId ? buttonId : '#button';

    var cont = true;
    if(confirmation)
        cont = confirm(confirmation);

    if(cont == true){
        
        $(buttonId).prop('disabled', 'disabled');
        $(buttonId).html('Loading..');

        $(formId).submit();
    }

}